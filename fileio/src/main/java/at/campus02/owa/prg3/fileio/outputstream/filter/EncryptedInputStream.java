package at.campus02.owa.prg3.fileio.outputstream.filter;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class EncryptedInputStream extends FilterInputStream {
	
	private final int key;

	public EncryptedInputStream(InputStream arg0, int key) {
		super(arg0);
		this.key = key;
	}

	@Override
	public int read() throws IOException {
		int result = super.read();
		if (result == -1) {
			return -1;
		}
		return result ^ key;
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		byte[] buffer = new byte[len];
		int result = super.read(buffer, off, len);
		for (int i = 0; i < len; i++) {
			b[i] = (byte) (buffer[i] ^ key);
		}
		return result;
	}

	@Override
	public int read(byte[] b) throws IOException {
		byte[] buffer = new byte[b.length];
		int result = super.read(buffer);
		for (int i = 0; i < b.length; i++) {
			b[i] = (byte) (buffer[i] ^ key);
		}
		return result;
	}
	
	

}
