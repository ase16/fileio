package at.campus02.owa.prg3.fileio.outputstream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import at.campus02.owa.prg3.fileio.outputstream.filter.ByteCountOutputStream;
import at.campus02.owa.prg3.fileio.outputstream.filter.EncryptedInputStream;
import at.campus02.owa.prg3.fileio.outputstream.filter.EncryptedOutputStream;

public class FilterStreamDemo {

	public static void main(String[] args) throws IOException {
		int key = 60;
		File file = new File("streams.txt");
		FileOutputStream fos = new FileOutputStream(file);
		ByteCountOutputStream bcos = new ByteCountOutputStream(fos);
		EncryptedOutputStream eos = new EncryptedOutputStream(bcos, key);
		for (int i = 32; i <= 126; i++) {
			eos.write(i);
		}
		String inhalt = "\nHallo Welt!";
		char[] ch = inhalt.toCharArray();
		for (char c : ch) {
			eos.write((int)c);
		}
		eos.flush();
		eos.close();
		System.out.println("Geschriebene Bytes: " + bcos.getCounter());
		
		EncryptedInputStream eis = new EncryptedInputStream(new FileInputStream(file), key);
		int byteRead;
		while ((byteRead = eis.read()) != -1) {
			char[] cha = Character.toChars(byteRead);
			System.out.print(cha[0]);
		}
		eis.close();
	}

}
