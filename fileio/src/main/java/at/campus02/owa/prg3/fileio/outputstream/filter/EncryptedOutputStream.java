package at.campus02.owa.prg3.fileio.outputstream.filter;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class EncryptedOutputStream extends FilterOutputStream {
	
	private final int key; 

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		byte[] buffer = new byte[b.length];
		for (int i = 0; i < b.length; i++) {
			buffer[i] = (byte) (b[i] ^ key);
		}
		super.write(buffer, off, len);
	}

	@Override
	public void write(byte[] b) throws IOException {
		byte[] buffer = new byte[b.length];
		for (int i = 0; i < b.length; i++) {
			buffer[i] = (byte) (b[i] ^ key);
		}
		super.write(buffer);
	}

	@Override
	public void write(int b) throws IOException {
		super.write(b ^ key);
	}

	public EncryptedOutputStream(OutputStream out, int key) {
		super(out);
		this.key = key;
	}

}
