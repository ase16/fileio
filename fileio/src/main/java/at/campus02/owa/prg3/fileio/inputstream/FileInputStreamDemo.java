package at.campus02.owa.prg3.fileio.inputstream;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileInputStreamDemo {

	public static void main(String[] args) throws IOException {
		File input = new File("input.txt");
		FileInputStream fis = new FileInputStream(input);
		int byteRead;
		while ((byteRead = fis.read()) != -1) {
			char[] ch = Character.toChars(byteRead);
			System.out.print(ch[0]);
		}
		fis.close();
	}

}
