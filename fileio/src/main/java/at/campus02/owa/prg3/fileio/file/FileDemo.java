package at.campus02.owa.prg3.fileio.file;

import java.io.File;
import java.io.IOException;

public class FileDemo {

	public static void main(String[] args) throws IOException {
		File demo = new File("demo.txt");
		/* Prüfen, ob Datei existiert */
		if (!demo.exists()) {
			/* Datei wird leer erzeugt wenn sie nicht existiert */
			demo.createNewFile();
		}
		/* Einige Informationen zur Datei ausgeben */
		System.out.println("Ordner: " + demo.getAbsoluteFile().getParent());
		System.out.println("Größe: " + demo.length());
		System.out.println("Inhalt des Ordners:");
		/* Alle Dateien im selben Verzeichnis auflisten */
		for (File f : demo.getAbsoluteFile().getParentFile().listFiles()) {
			System.out.println(" * " + f.getName());
		}
	}

}
