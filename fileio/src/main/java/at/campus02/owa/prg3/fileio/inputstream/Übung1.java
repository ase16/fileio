package at.campus02.owa.prg3.fileio.inputstream;

import java.io.IOException;

public class Übung1 {

	public static void main(String[] args) throws IOException {
		int byteRead;
		while ((byteRead = System.in.read()) != -1) {
			String inp = String.valueOf(Character.toChars(byteRead));
			if (inp.equals("x")) {
				break;
			}
			System.out.print(inp);
		}
	}

}
