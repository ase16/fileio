package at.campus02.owa.prg3.fileio.outputstream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Übung2 {

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		String inhalt = "Hallo Datei, dies ist ein Text.";
		File file = new File("object.dat");
		if (file.exists()) {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
			String gelesen = (String)ois.readObject();
			if (gelesen.equals(inhalt)) {
				System.out.println("Super, richtig gelesen!");
			} else {
				System.out.println("Alles im Eimer!");
			}
			ois.close();
		} else {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
			oos.writeObject(inhalt);
			oos.flush();
			oos.close();
		}

	}

}
