package at.campus02.owa.prg3.fileio.outputstream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutputStreamDemo {

	public static void main(String[] args) throws IOException {
		File file = new File("output.txt");
		if (file.exists()) {
			FileInputStream fis = new FileInputStream(file);
			int byteRead;
			while ((byteRead = fis.read()) != -1) {
				char[] ch = Character.toChars(byteRead);
				System.out.print(ch[0]);
			}
			fis.close();
		} else {
			file.createNewFile();
			FileOutputStream fos = new FileOutputStream(file);
			for (int i = 32; i <= 126; i++) {
				fos.write(i);
			}
			String inhalt = "\nHallo Welt!";
			char[] ch = inhalt.toCharArray();
			for (char c : ch) {
				fos.write((int)c);
			}
			fos.flush();
			fos.close();
		}

	}

}
